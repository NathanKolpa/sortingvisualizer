#include <Core.h>
#include <iostream>
#include <Windows.h>

#include "Core/GUI/Button/Button.h"
#include "Sorting/Algorithms/BubbleSort.h"

class MyGameState : public orb::GameState
{
private:
	BubbleSort m_sortingAlgo;
public:
	MyGameState(orb::Window* window)
		:GameState(window), m_sortingAlgo(10000)
	{

		orb::input::ApplicationEvent* appEvent = new orb::input::ApplicationEvent(m_window);
		appEvent->setOnCloseEvent([&](sf::RenderWindow* window)
		{
			m_sortingAlgo.kill();
			window->close();
		});

		orb::input::KeyboardEvent* shuffle = new orb::input::KeyboardEvent(sf::Keyboard::BackSpace);
		shuffle->setOnReleaseCallback([&]()
		{
			m_sortingAlgo.shuffle();
		});

		m_sortingAlgo.start();
		m_eventHandle.addEvent(shuffle);
		m_eventHandle.addEvent(appEvent);
	}
protected:

	void gameLogic()
	{
	}

	void render()
	{
		m_sortingAlgo.draw();
	}
};

int main()//array van rects
{
	{

		orb::Window window(1280, 720, "Sorting Visualizer");
		window.setVerticalSyncEnabled(true);
		MyGameState testGame(&window);

		testGame.run([&]
		{
			return window.isActive();
		});
	}

}