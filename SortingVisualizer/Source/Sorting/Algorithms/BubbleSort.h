#pragma once
#include "../SortingAlgorithm.h"

class BubbleSort : public SortingAlgorithm
{
private:
	void sort()
	{
		int i, j;
		for (i = 0; i < getSize() - 1; i++)

			for (j = 0; j < getSize() - i - 1; j++)
				if (getValue(j) > getValue(j + 1))
					swap(j, j + 1);

		finishSorting();
	}
public:
	BubbleSort(int size)
		:SortingAlgorithm(size)
	{
	}
};