#include "SortingAlgorithm.h"
#include <SFML/OpenGL.hpp>
#include <Core.h>
#include <iostream>

SortingAlgorithm::SortingAlgorithm(int size)
{
	m_array.resize(size);
	m_lastArray.resize(size);
	m_size = size;

	for (int i = 0; i < m_size; i++)
	{
		m_array[i] = i;
	}

	shuffle();
}

SortingAlgorithm::~SortingAlgorithm()
{
	kill();
}

void SortingAlgorithm::arrayAccsess()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(m_delay));
}

std::vector<int>& SortingAlgorithm::getArray()
{
	return m_array;
}

int SortingAlgorithm::getSize()
{
	return m_size;
}

int SortingAlgorithm::getValue(int index)
{
#if defined(_DEBUG)
	if (index >= m_size || index < 0)
		std::cout << "invalid array acces" << std::endl;
#endif 

	std::lock_guard<std::mutex> lk(m_lock);
	return m_array[index];
}

void SortingAlgorithm::setValue(int index, int value)
{
	arrayAccsess();
	std::lock_guard<std::mutex> lk(m_lock);
	m_array[index] = value;
}

void SortingAlgorithm::swap(int index1, int index2)
{
	arrayAccsess();
	std::lock_guard<std::mutex> lk(m_lock);

	int value2 = m_array[index2];
	m_array[index2] = m_array[index1];
	m_array[index1] = value2;
}


void SortingAlgorithm::finishSorting()
{
	bool succsess = true;
	for (int i = 0; i < m_size; i++)
	{
		if (m_array[i] != i)
		{
			std::cout << "unsucsessfull sort!";
			succsess = false;
			break;
		}
	}

	if (succsess)
		std::cout << "sucsessfull sort" << std::endl;
}

void SortingAlgorithm::shuffle()
{
	std::lock_guard<std::mutex> lk(m_lock);
	for (int i = 0; i < m_size; i++)
	{
		int rand = orb::utils::random<int>(0, m_size - 1);

		int value2 = m_array[rand];
		m_array[rand] = m_array[i];
		m_array[i] = value2;
	}
}

void SortingAlgorithm::draw()
{//get a local array???

	float step = 2.0f / (float)m_size;
	float originX = -1.0f - step;

	for (int i = 0; i < m_size; i++)
	{
		originX += step;
		int lenght;

		{
			std::lock_guard<std::mutex> lk(m_lock);
			lenght = m_array[i];
		}

		float height = step * (float)lenght - 1.0f;

		if (lenght == m_lastArray[i])
		{
			glColor4f(1, 1, 1, 1);
		}
		else
		{
			glColor4f(1, 0, 0, 1);
		}


		glBegin(GL_QUADS);
		glVertex2f(originX, height);
		glVertex2f(originX, -1);
		glVertex2f(originX + step, -1);
		glVertex2f(originX + step, height);
		glEnd();

		/*
		glColor4f(0.3, 0.3, 0.3, 1);
		glBegin(GL_LINES);
		glVertex2f(originX, -1);
		glVertex2f(originX, height);
		glEnd();
		*/
	}

	std::lock_guard<std::mutex> lk(m_lock);
	m_lastArray = m_array;
}

void SortingAlgorithm::start()
{
	m_sortingThread = new std::thread(&SortingAlgorithm::sort, this);
}

void SortingAlgorithm::kill()
{
	if (m_sortingThread != nullptr)
	{
		m_delay = 0;
		if (m_sortingThread->joinable())
			m_sortingThread->join();
	}
}
