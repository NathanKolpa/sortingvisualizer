#pragma once
#include <thread>
#include <mutex>
#include <vector>

class SortingAlgorithm
{
private:
	std::mutex m_lock;
	std::vector<int> m_array;
	std::vector<int> m_lastArray;
	std::thread* m_sortingThread = nullptr;
	int m_size;
	int m_delay = 0;
public:
	SortingAlgorithm(int size);
	~SortingAlgorithm();
private:
protected:

	std::vector<int>& getArray();
	int getSize();
	int getValue(int index);
	void setValue(int index, int value);
	void swap(int index1, int index2);//array van sorting element

	void arrayAccsess();

	void finishSorting();
	virtual void sort() = 0;

public:
	void shuffle();
	void draw();
	void start();
	void kill();
};