#pragma once
#include <SFML/Graphics.hpp>
#include "../Input/Event.h"
#include <string>
#include <functional>
#include "../ResourceLoader/FontResource.h"

namespace orb
{
	namespace gui
	{
		class GuiElement
		{
		protected:
			std::string m_innerText;
			sf::Vector2f m_position;
			sf::Vector2f m_size;
			float m_paddingLeft = 10, m_paddingRight = 10, m_paddingTop = 10, m_paddingBottom = 10;
			resource::FontResource* m_fontRes;
		public:
			GuiElement();
			~GuiElement();
		protected:
			virtual void updateElement(){}
		public:
			virtual void draw(sf::RenderWindow* window) {};

			// I know get setters are a sin.
			std::string getInnerText();
			void setInnerText(std::string text);

			sf::Vector2f getPosition();//mischien weg?
			void setPosition(sf::Vector2f position);

			sf::Vector2f getSize();//mischien weg?
			void setSize(sf::Vector2f size);

			void setFont(std::string fontFile);
			void setFont(resource::FontResource* fontResource);
		};
	}
}