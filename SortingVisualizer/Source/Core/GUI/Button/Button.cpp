#include "Button.h"

orb::gui::Button::Button()
{
}

orb::gui::Button::~Button()
{
}

void orb::gui::Button::updateElement()
{
	float border = 3;

	m_rect.setPosition(m_position);
	m_rect.setOutlineThickness(border);
	m_rect.setOutlineColor(sf::Color::Black);

	m_text.setFont(m_fontRes->getFont());					//alles regeneraten on text change
	m_text.setString(sf::String(m_innerText));
	m_text.setPosition(m_position + sf::Vector2f(m_paddingLeft, m_paddingTop - m_text.getGlobalBounds().height / 2 + border));
	m_text.setFillColor(sf::Color::Red);

	sf::Vector2f size = m_size;
	size += sf::Vector2f(m_paddingLeft + m_paddingRight, m_paddingTop + m_paddingBottom) + sf::Vector2f(m_text.getLocalBounds().width, m_text.getLocalBounds().height);

	m_rect.setSize(size);
}

void orb::gui::Button::draw(sf::RenderWindow* window)
{

	if (!m_fontRes->isLoaded())
	{
		m_fontRes->loadDefault();
		updateElement();
	}


	sf::Vector2i mouse = sf::Mouse::getPosition(*window);
	handleEvents(mouse, m_rect.getPosition(), m_rect.getSize());

	window->draw(m_rect);
	window->draw(m_text);
}
