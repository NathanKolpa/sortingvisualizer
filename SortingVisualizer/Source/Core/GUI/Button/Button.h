#pragma once
#include "../ClickebleElement.h"

namespace orb
{
	namespace gui
	{
		class Button : public ClickebleElement
		{
		private:
			sf::RectangleShape m_rect;
			sf::Text m_text;
		public:
			Button();
			~Button();
		private:
			void updateElement();
		public:
			void draw(sf::RenderWindow* window);
		};
	}
}