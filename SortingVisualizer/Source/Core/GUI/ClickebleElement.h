#pragma once
#include "GuiElement.h"

namespace orb
{
	namespace gui
	{
		class ClickebleElement : public  GuiElement
		{
		private:
			bool current = false;
			bool last = false;
		protected:
			std::function<void()> m_hoverCallback, m_clickCallback;
			void handleEvents(sf::Vector2i mouse, sf::Vector2f position, sf::Vector2f size);
		public:
			void setOnHoverEvent(std::function<void()> callback) { m_hoverCallback = callback; }
			void setOnClickEvent(std::function<void()> callback) { m_clickCallback = callback; }
		};
	}
}