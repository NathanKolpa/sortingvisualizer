#include "ClickebleElement.h"

void orb::gui::ClickebleElement::handleEvents(sf::Vector2i mouse, sf::Vector2f position, sf::Vector2f size)
{
	if (m_hoverCallback == nullptr && m_clickCallback == nullptr)
		return;

	if (mouse.x > position.x && mouse.x < position.x + size.x
		&& mouse.y > position.y && mouse.y < position.y + size.y)
	{

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			current = true;
		}
		else
		{
			current = false;

			if (m_hoverCallback != nullptr)
				m_hoverCallback();

			if (last)
			{
				if (m_clickCallback != nullptr)
				{
					m_clickCallback();
				}
			}

			if (last && m_clickCallback != nullptr || m_hoverCallback != nullptr)
				updateElement();
		}
	}

	last = current;
}
