#include "GuiElement.h"

orb::gui::GuiElement::GuiElement()
{
	m_fontRes = new resource::FontResource;
}

orb::gui::GuiElement::~GuiElement()
{
}

std::string orb::gui::GuiElement::getInnerText()
{
	return m_innerText;
}
void orb::gui::GuiElement::setInnerText(std::string text)
{
	m_innerText = text;
	updateElement();
}
sf::Vector2f orb::gui::GuiElement::getPosition()
{
	return m_position;
}
void orb::gui::GuiElement::setPosition(sf::Vector2f position)
{
	m_position = position;
	updateElement();
}
sf::Vector2f orb::gui::GuiElement::getSize()
{
	return m_size;
}
void orb::gui::GuiElement::setSize(sf::Vector2f size)
{
	m_size = size;
	updateElement();
}


void orb::gui::GuiElement::setFont(std::string fontFile)
{
	m_fontRes->load(fontFile);
}

void orb::gui::GuiElement::setFont(resource::FontResource* fontResource)
{
	m_fontRes = fontResource;
}

