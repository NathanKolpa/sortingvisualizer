#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include <chrono>

namespace orb
{
	class Window : public sf::RenderWindow
	{
	private:
		int m_windowWidth, m_windowHeight;
		std::string m_windowName;
		bool m_isActive = false;

		std::chrono::steady_clock::time_point m_previousTime;
		std::chrono::steady_clock::time_point m_currentTime;
		double m_deltaTime = 0;
		unsigned int _framecount = 0;
		unsigned int _totalfps = 0;
		double _elapsedTime = 0;
	public:
		int getWidth();
		int getHeight();
	public:
		Window(int width, int height, std::string name);
		~Window();
	private:
		void handleTime();
	public:
		bool isActive();
		void prepare();
		void update();
	};
}
