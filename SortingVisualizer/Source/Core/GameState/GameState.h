#pragma once
#include "../Window/Window.h"
#include <functional>
#include "../Input/EventHandle.h"


namespace orb
{
	class GameState
	{
	private:
		bool isOpen = true;
	protected:
		Window* m_window;
		input::EventHandle m_eventHandle;
	public:
		GameState(Window* window);
	private:
		void handleInput();
	public:
		void run(std::function<bool()>runCondition);
	protected:
		void close();
		void addBasicEvents();//not recomended to use

		//optinonal method when run is called
		virtual void init() {};

		//you must override this
		virtual void gameLogic() = 0;
		virtual void render() = 0;
	};
}