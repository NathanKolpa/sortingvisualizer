#include "GameState.h"
#include "../Input/ApplicationEvent.h"
#include <SFML/OpenGL.hpp>

orb::GameState::GameState(Window* window)
{
	m_window = window;
}

void orb::GameState::run(std::function<bool()> runCondition)
{
	init();

	while (runCondition() && isOpen)
	{
		m_window->prepare();

		handleInput();
		gameLogic();
		render();

		m_window->update();
	}
}

void orb::GameState::close()
{
	isOpen = false;
}

void orb::GameState::addBasicEvents()
{
	orb::input::ApplicationEvent* applicationEvents = new orb::input::ApplicationEvent(m_window);
	applicationEvents->setOnCloseEvent([&](sf::RenderWindow* window)
	{
		m_window->close();
	});

	applicationEvents->setOnWindowResizeEvent([&](int x, int y)
	{
		glViewport(0, 0, x, y);//TODO: GLfnc
	});


	m_eventHandle.addEvent(applicationEvents);
}

void orb::GameState::handleInput()
{
	m_eventHandle.handleAllEvents();
}
