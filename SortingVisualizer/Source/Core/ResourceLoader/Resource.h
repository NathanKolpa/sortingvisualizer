#pragma once
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

namespace orb
{
	namespace resource
	{
		class Resource
		{
		private:
			const std::string m_beginPath = "Resource/";
			std::string m_folder;
		public:
			Resource(std::string folder);
			~Resource();
		protected:
			bool loaded = false;
			std::string getPath();
			void errorFailToLoad(std::string filePath);
		public:
			bool isLoaded();
			void clear();
			int getSizeOfBytes();

			virtual void loadDefault() {}
			virtual bool load(std::string file) { return false; }
		};
	}
}