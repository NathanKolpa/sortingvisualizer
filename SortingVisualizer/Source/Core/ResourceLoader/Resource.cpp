#include "Resource.h"
#include <iostream>

orb::resource::Resource::Resource(std::string folder)
{
	m_folder = folder;
}

orb::resource::Resource::~Resource()
{
}

std::string orb::resource::Resource::getPath()
{
	return m_beginPath + m_folder + "/";
}

void orb::resource::Resource::errorFailToLoad(std::string filePath)
{
	std::cout << "failed to load resource: " << filePath << std::endl;
}

bool orb::resource::Resource::isLoaded()
{
	return loaded;
}
