#include "FontResource.h"
#include <fstream>
#include <iostream>
#include <iomanip>

orb::resource::FontResource::FontResource()
	:Resource("Fonts")
{
}

orb::resource::FontResource::FontResource(std::string file)
	:Resource("Fonts")
{
	load(file);
}

orb::resource::FontResource::~FontResource()
{
}

bool orb::resource::FontResource::loadFont(std::string file)
{
	if (!m_font.loadFromFile(getPath() + file))
	{
		loadDefault();

		return false;
	}
	return true;
}

bool orb::resource::FontResource::load(std::string file)
{
	loaded = true;

	return loadFont(file);
}

void orb::resource::FontResource::loadDefault()
{
	loaded = true;
	m_font.loadFromFile("C:/Windows/Fonts/arial.ttf");
}

sf::Font & orb::resource::FontResource::getFont()
{
	return m_font;
}
