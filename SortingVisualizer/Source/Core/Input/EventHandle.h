#pragma once
#include "Event.h"
#include <vector>

namespace orb
{
	namespace input
	{
		class EventHandle
		{
		private:
			std::vector<Event*> m_events;
		public:
			EventHandle();
			~EventHandle();
		public:
			void addEvent(Event* event);
			void handleAllEvents();
		};
	}
}