#pragma once
#include "Event.h"

namespace orb
{
	namespace input
	{
		class ApplicationEvent : public Event
		{
		private:
			std::function<void(sf::RenderWindow*)> m_closeCallback;
			std::function<void(int, int)> m_resizeCallback;
			sf::RenderWindow* m_window;
		public:
			ApplicationEvent(sf::RenderWindow* window);
			~ApplicationEvent();
		private:
			void handleState();
		public:
			void setOnCloseEvent(std::function<void(sf::RenderWindow*)> callback);
			void setOnWindowResizeEvent(std::function<void(int, int)> callback);
		};
	}
}