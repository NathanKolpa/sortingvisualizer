#pragma once
#include "Event.h"

namespace orb
{
	namespace input
	{
		class KeyboardEvent : public Event
		{
		private:
			sf::Keyboard::Key m_keycode;
			std::function<void()> m_holdCallback, m_pressCallback, m_releaseCallback;
		public:
			KeyboardEvent(sf::Keyboard::Key keycode);
			~KeyboardEvent();
		private:
			void handleState();
		public:
			void setOnHoldCallback(std::function<void()> callback);
			void setOnPressCallback(std::function<void()> callback);
			void setOnReleaseCallback(std::function<void()> callback);
		};
	}
}