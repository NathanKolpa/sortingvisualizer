#include "MouseButtonEvent.h"

orb::input::MouseButtonEvent::MouseButtonEvent(sf::Mouse::Button button)
{
	m_button = button;
}

orb::input::MouseButtonEvent::~MouseButtonEvent()
{
}

void orb::input::MouseButtonEvent::handleState()
{
	if (sf::Mouse::isButtonPressed(m_button))
	{
		currentState = true;

		if (m_holdCallback != nullptr)
			m_holdCallback();
	}
	else
	{
		currentState = false;
	}

	if (currentState)
	{
		if (!lastState && m_clickCallback != nullptr)
		{
			m_clickCallback();
		}
	}
	else
	{
		if (lastState && m_releaseCallback != nullptr)
		{
			m_releaseCallback();
		}
	}
}

void orb::input::MouseButtonEvent::setOnHoldCallback(std::function<void()> callback)
{
	m_holdCallback = callback;
}

void orb::input::MouseButtonEvent::setOnClickCallback(std::function<void()> callback)
{
	m_clickCallback = callback;
}

void orb::input::MouseButtonEvent::setOnReleaseCallback(std::function<void()> callback)
{
	m_releaseCallback = callback;
}
