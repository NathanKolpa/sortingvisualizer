#include "MouseEvent.h"

orb::input::MouseEvent::MouseEvent(sf::RenderWindow* window)
{
	m_window = window;
}

orb::input::MouseEvent::~MouseEvent()
{
}

void orb::input::MouseEvent::handleState()
{
	sf::Vector2i current = sf::Mouse::getPosition(*m_window);

	if (current != m_lastMouse)
	{//the mouse moved
		if (m_mouseMoveCallback != nullptr)
			m_mouseMoveCallback(current.x, current.y);
	}

	m_lastMouse = current;
}

void orb::input::MouseEvent::setOnMouseMoveEvent(std::function<void(int, int)> callback)
{
	m_mouseMoveCallback = callback;
}
