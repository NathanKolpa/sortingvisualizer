#include "KeyboardEvent.h"

orb::input::KeyboardEvent::KeyboardEvent(sf::Keyboard::Key keycode)
{
	m_keycode = keycode;
}

orb::input::KeyboardEvent::~KeyboardEvent()
{
}

void orb::input::KeyboardEvent::handleState()
{
	if (sf::Keyboard::isKeyPressed(m_keycode))
	{
		if (lastState && m_holdCallback != nullptr)
			m_holdCallback();

		currentState = true;
	}
	else
	{
		currentState = false;
	}

	if (currentState)
	{
		if (!lastState && m_pressCallback != nullptr)
		{
			m_pressCallback();
		}
	}
	else
	{
		if (lastState && m_releaseCallback != nullptr)
		{
			m_releaseCallback();
		}
	}
}

void orb::input::KeyboardEvent::setOnHoldCallback(std::function<void()> callback)
{
	m_holdCallback = callback;
}

void orb::input::KeyboardEvent::setOnPressCallback(std::function<void()> callback)
{
	m_pressCallback = callback;
}

void orb::input::KeyboardEvent::setOnReleaseCallback(std::function<void()> callback)
{
	m_releaseCallback = callback;
}
