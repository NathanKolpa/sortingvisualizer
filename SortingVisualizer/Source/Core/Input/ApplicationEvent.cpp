#include "ApplicationEvent.h"

orb::input::ApplicationEvent::ApplicationEvent(sf::RenderWindow* window)
{
	m_window = window;
}

orb::input::ApplicationEvent::~ApplicationEvent()
{
}

//enable disable means focus
void orb::input::ApplicationEvent::handleState()
{

	sf::Event event;
	while (m_window->pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			if (m_closeCallback != nullptr)
				m_closeCallback(m_window);
			break;

		case sf::Event::Resized:
			if (m_resizeCallback != nullptr)
				m_resizeCallback(event.size.width, event.size.height);
			break;
		}
	}
}

void orb::input::ApplicationEvent::setOnCloseEvent(std::function<void(sf::RenderWindow*)> callback)
{
	m_closeCallback = callback;
}

void orb::input::ApplicationEvent::setOnWindowResizeEvent(std::function<void(int, int)> callback)
{
	m_resizeCallback = callback;
}
