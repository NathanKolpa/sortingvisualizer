#include "Event.h"

orb::input::Event::Event()
{
}

orb::input::Event::~Event()
{
}

void orb::input::Event::handle()
{
	handleState();

	lastState = currentState;
}
