#include "EventHandle.h"

orb::input::EventHandle::EventHandle()
{
}

orb::input::EventHandle::~EventHandle()
{
	for (int i = 0; i < (int)m_events.size(); i++)
	{
		delete m_events[i];
	}
}

void orb::input::EventHandle::addEvent(Event* event)
{
	m_events.push_back(event);
}

void orb::input::EventHandle::handleAllEvents()
{
	for (int i = 0; i < (int)m_events.size(); i++)
	{
		m_events[i]->handle();
	}
}
