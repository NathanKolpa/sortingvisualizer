#pragma once
#include "Event.h"

namespace orb
{
	namespace input
	{

		class MouseEvent : public Event
		{
		private:
			std::function<void(int, int)> m_mouseMoveCallback;
			sf::RenderWindow* m_window;
			sf::Vector2i m_lastMouse;
		public:
			MouseEvent(sf::RenderWindow* window);
			~MouseEvent();
		private:
			void handleState();
		public:
			void setOnMouseMoveEvent(std::function<void(int, int)> callback);
		};
	}
}