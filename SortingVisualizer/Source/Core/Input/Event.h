#pragma once
#include <functional>
#include <SFML/Graphics.hpp>

namespace orb
{
	namespace input
	{
		class Event
		{
		protected:
			bool lastState = false;
			bool currentState = false;
		public:
			Event();
			~Event();
		protected:
			virtual void handleState() {}
		public:
			void handle();
		};
	}
}