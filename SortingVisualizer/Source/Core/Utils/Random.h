#pragma once
#include<ctime>
#include <random>
#include <limits>

static std::default_random_engine _generator((unsigned int)time(NULL));

namespace orb
{
	namespace utils
	{
		//random number between x and y
		template<typename T>
		T random(T min, T max)
		{
			std::uniform_int_distribution<T> distribution(min, max);
			return distribution(_generator);
		}

		//random number between 0 and x
		template<typename T>
		T random(T max)
		{
			std::uniform_int_distribution<T> distribution(0, max);
			return distribution(_generator);
		}

		//random num between 0 and y
		template<typename T>
		T random()
		{
			int max = std::numeric_limits<T>::max();
			std::uniform_int_distribution<T> distribution(0, max);
			return distribution(_generator);
		}
	}
}